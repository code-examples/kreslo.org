# [kreslo.org](https://kreslo.org) source codes

<br/>

### Run kreslo.org on localhost

    # vi /etc/systemd/system/kreslo.org.service

Insert code from kreslo.org.service

    # systemctl enable kreslo.org.service
    # systemctl start kreslo.org.service
    # systemctl status kreslo.org.service

http://localhost:4035
